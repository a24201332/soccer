<h1>HOSPITALES</h1>

<?php if ($listadoHospitales): ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>DIRECCION</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoHospitales as $hospital): ?>
        <tr>
          <td><?php echo $hospital->id_hos; ?></td>
          <td><?php echo $hospital->nombre_hos; ?></td>
          <td><?php echo $hospital->direccion_hos; ?></td>
          <td>
          <a href="<?php echo site_url('hospitales/borrar/').$hospital->id_hos; ?>">Eliminar</a>
        </td>
        </tr>

      <?php endforeach; ?>
    </tbody>
  </table>


<?php else: ?>
  <div class="alert alert-danger">
    NO SE ENCONTRO HOSPITALES REGISTRADOS

  </div>

<?php endif; ?>
