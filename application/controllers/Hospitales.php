<?php
class Hospitales extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Hospital");
  }
  //renderizacion de la vista index a hospitales
  public function index(){
    $data["listadoHospitales"]=$this->Hospital->consultarTodos();
    $this->load->view("header");
    $this->load->view("hospitales/index",$data);
    $this->load->view("footer");
  }

  //eliminacion de hospitales recibiendo el id por get
  public function borrar($id_hos){
    $this->Hospital->eliminar($id_hos);
    redirect("hospitales/index");
    

  }
}//fin de la clase












 ?>
